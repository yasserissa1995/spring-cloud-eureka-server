package com.yasser.registry.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaServer
@EnableConfigServer
@EnableFeignClients
public class EurekaServerApplication {
   public static void main(String[] args) {

      SpringApplication.run(EurekaServerApplication.class, args);
   }
}