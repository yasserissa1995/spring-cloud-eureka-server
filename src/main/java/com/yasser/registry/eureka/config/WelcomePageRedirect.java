//package com.yasser.registry.eureka.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.Ordered;
//import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@Configuration
//public class WelcomePageRedirect implements WebMvcConfigurer {
//
//   @Override
//   public void addResourceHandlers(ResourceHandlerRegistry registry) {
//      registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
//      registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
//   }
//}