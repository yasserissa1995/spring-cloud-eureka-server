//package com.yasser.registry.eureka.web.rest.error;
//
//import javassist.tools.web.BadHttpRequest;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.cloud.config.server.resource.NoSuchResourceException;
//import org.springframework.context.MessageSource;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.client.HttpClientErrorException;
//import org.springframework.web.servlet.ModelAndView;
//import org.springframework.web.servlet.NoHandlerFoundException;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//
//import java.util.Locale;
//
//@ControllerAdvice
//@EnableWebMvc
//@RequiredArgsConstructor
//@Slf4j
//public class CustomExceptionHandler {
//   //   ResponseEntityExceptionHandler
//   private final MessageSource messageSource;
//
//   private final Locale EN = Locale.ENGLISH;
//   private final String TITLE_KEY = "title";
//   private final String SUB_TITLE_KEY = "sub_title";
//   private final String CLASS_NAME_KEY = "class_name";
//   private final String ERROR_CODE = "error_code";
//
//   @ExceptionHandler(value = {NoHandlerFoundException.class, NoSuchResourceException.class})
//   public ModelAndView pageNotFound() {
//      log.error("Error page not found {}");
//      return buildModel(404);
//   }
//
//
//   @ExceptionHandler(HttpClientErrorException.BadRequest.class)
//   public ModelAndView HttpClientErrorException() {
//      log.error("Error BadRequestException request : {}");
//      return buildModel(404);
//   }
//
//   @ExceptionHandler(BadHttpRequest.class)
//   public ModelAndView BadHttpRequest() {
//      log.error("Error BadHttpRequest request : {}");
//      return buildModel(404);
//   }
//
//
//   @ExceptionHandler(Exception.class)
//   public ModelAndView exception(Exception exception) {
//      log.error("Error Exception : {}");
//
//      return buildModel();
//   }
//
//   private ModelAndView buildModel(int... errorCode) {
//      log.error("Error : with code : {}", errorCode);
//
//      ModelAndView modelAndView = new ModelAndView();
//      modelAndView.setViewName("error");
//
//      if (errorCode == null || errorCode.length == 0) {
//         errorCode = new int[]{1000};
//      }
//
//      switch (errorCode[0]) {
//         case 400:
//            modelAndView.addObject(CLASS_NAME_KEY, messageSource.getMessage("error.bad-request.class-name", null, EN));
//            modelAndView.addObject(TITLE_KEY, messageSource.getMessage("error.bad-request.title", null, EN));
//            modelAndView.addObject(SUB_TITLE_KEY, messageSource.getMessage("error.bad-request.sub-title", null, EN));
//            modelAndView.addObject(ERROR_CODE, errorCode[0]);
//            break;
//
//         case 401:
//            modelAndView.addObject(CLASS_NAME_KEY, messageSource.getMessage("error.unauthorized.class-name", null, EN));
//            modelAndView.addObject(TITLE_KEY, messageSource.getMessage("error.unauthorized.title", null, EN));
//            modelAndView.addObject(SUB_TITLE_KEY, messageSource.getMessage("error.unauthorized.sub-title", null, EN));
//            modelAndView.addObject(ERROR_CODE, errorCode[0]);
//            break;
//
//         case 403:
//            modelAndView.addObject(CLASS_NAME_KEY, messageSource.getMessage("error.forbidden.class-name", null, EN));
//            modelAndView.addObject(TITLE_KEY, messageSource.getMessage("error.forbidden.title", null, EN));
//            modelAndView.addObject(SUB_TITLE_KEY, messageSource.getMessage("error.forbidden.sub-title", null, EN));
//            modelAndView.addObject(ERROR_CODE, errorCode[0]);
//            break;
//
//         case 404:
//            modelAndView.addObject(CLASS_NAME_KEY, messageSource.getMessage("error.not-found.class-name", null, EN));
//            modelAndView.addObject(TITLE_KEY, messageSource.getMessage("error.not-found.title", null, EN));
//            modelAndView.addObject(SUB_TITLE_KEY, messageSource.getMessage("error.not-found.sub-title", null, EN));
//            modelAndView.addObject(ERROR_CODE, errorCode[0]);
//            break;
//
//         case 405:
//            modelAndView.addObject(CLASS_NAME_KEY, messageSource.getMessage("error.method-not-allowed.class-name", null, EN));
//            modelAndView.addObject(TITLE_KEY, messageSource.getMessage("error.method-not-allowed.title", null, EN));
//            modelAndView.addObject(SUB_TITLE_KEY, messageSource.getMessage("error.method-not-allowed.sub-title", null, EN));
//            modelAndView.addObject(ERROR_CODE, errorCode[0]);
//            break;
//
//         case 500:
//            modelAndView.addObject(CLASS_NAME_KEY, messageSource.getMessage("error.internal-server-error.class-name", null, EN));
//            modelAndView.addObject(TITLE_KEY, messageSource.getMessage("error.internal-server-error.title", null, EN));
//            modelAndView.addObject(SUB_TITLE_KEY, messageSource.getMessage("error.internal-server-error.sub-title", null, EN));
//            modelAndView.addObject(ERROR_CODE, errorCode[0]);
//            break;
//
//         default:
//            modelAndView.addObject(CLASS_NAME_KEY, messageSource.getMessage("error.unhandled.class-name", null, EN));
//            modelAndView.addObject(TITLE_KEY, messageSource.getMessage("error.unhandled.title", null, EN));
//            modelAndView.addObject(SUB_TITLE_KEY, messageSource.getMessage("error.unhandled.sub-title", null, EN));
//            modelAndView.addObject(ERROR_CODE, 500);
//            break;
//      }
//
//      return modelAndView;
//   }
//
//}