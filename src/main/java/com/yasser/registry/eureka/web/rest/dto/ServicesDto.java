package com.yasser.registry.eureka.web.rest.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ServicesDto {

   private Applications applications;

   @Setter
   @Getter
   public static class Applications {

      private List<Application> application;


      @Setter
      @Getter
      public  static class Application {

         private String name;

         private List<Instance> instance;


         @Getter
         @Setter
         public static class Instance {

            private String instanceId;

            private String homePageUrl;

            private String statusPageUrl;

            private String healthCheckUrl;

         }
      }
   }
}