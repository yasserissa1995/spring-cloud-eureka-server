package com.yasser.registry.eureka.web.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yasser.registry.eureka.client.EurekaServiceClient;
import com.yasser.registry.eureka.web.rest.dto.ServicesDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/services")
@Slf4j
@RequiredArgsConstructor
public class ServiceResource {

   private final EurekaServiceClient eurekaServiceClient;
   private final ObjectMapper objectMapper;

   @GetMapping("/registered")
   public ResponseEntity<List<ServicesDto.Applications.Application>> getRegisteredServices() throws IOException {
      log.info("Rest request to get registered services");

      ResponseEntity<String> response = eurekaServiceClient.getRegisteredServices();

      if (response == null || response.getBody() == null) {
         return ResponseEntity.ok(new ArrayList<>());
      }

      ServicesDto servicesDto = objectMapper.readValue(response.getBody(), ServicesDto.class);

      return ResponseEntity.ok(servicesDto.getApplications().getApplication());
   }

}