package com.yasser.registry.eureka.client;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.yasser.registry.eureka.web.rest.dto.ServicesDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "registry-service", url = "http://localhost:8761")
public interface EurekaServiceClient {

   @GetMapping("/api/services/registered")
   ResponseEntity<String> getRegisteredServices() throws JsonProcessingException;

}